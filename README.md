# BioEye TestApp

BioEye TestApp is an Android application developed with Kotlin and follows the MVVM architecture. This application serves the purpose of determining lighting conditions using the device's light sensor, capturing frames from the device's camera using the CameraX Jetpack Library, integrating with Google ML Kit face recognition, saving a boolean indicating face recognition flag in a CSV file for each frame, and presenting a summary of the results to the user.

## Project Architecture
The primary focus of this project was the choice and implementation of the MVVM (Model-View-ViewModel) architecture. MVVM separates the user interface (View) from the business logic (ViewModel) and the underlying data (Model). This architectural choice was made to achieve better maintainability, testability, and code organization.

Trade-offs:

- Complexity vs. Maintainability: MVVM adds a layer of complexity compared to simpler architectures like MVP (Model-View-Presenter). However, the trade-off is better separation of concerns, which improves code maintainability and scalability.

## Kotlin as the Primary Language
Kotlin was chosen as the primary programming language for this project. Kotlin is a modern, expressive, and concise language that enhances developer productivity and safety.

Trade-offs:

- Language Choice vs. Compatibility: While Kotlin offers numerous benefits, it might require additional effort for developers who are more familiar with Java. However, the trade-off is improved code quality, readability, and reduced boilerplate code.

## Dependency Injection
KOIN is a lightweight and easy-to-use dependency injection framework for Kotlin applications. It simplifies the process of managing and providing dependencies across the application.

Trade-offs:

- Learning Curve vs. Productivity: While KOIN is user-friendly, there is still a learning curve involved in understanding its usage and best practices. The trade-off is improved code modularity and testability in exchange for the initial learning investment.

## Asynchronous Programming
Kotlin Flow and Coroutines were chosen for handling asynchronous operations, such as network requests and database transactions. Coroutines simplify asynchronous code and improve code readability, while Kotlin Flow enables reactive programming.

Trade-offs:

- Library Adoption vs. Control: The decision to use Coroutines and Kotlin Flow comes with the trade-off of introducing these libraries as dependencies. However, it greatly enhances the ability to handle concurrency and asynchronous operations, leading to more efficient and responsive applications.

## User Interface (UI) with ConstraintLayout
The user interface design and user experience were critical aspects of this project. Attention was given to creating an intuitive and visually appealing UI using ConstraintLayout.

Trade-offs:

- Complexity vs. UI Flexibility: ConstraintLayout offers a high level of flexibility in designing complex layouts. However, this flexibility can lead to increased layout complexity, which may require more effort to fine-tune and optimize. The trade-off is a responsive and adaptable UI that can accommodate various screen sizes and orientations.

## Third-party Libraries and Dependencies
The following third-party libraries and dependencies were used in this project:

- KOIN: Used for dependency injection to facilitate modularity and testability.
- Kotlin Coroutines: Utilized for asynchronous programming, improving the efficiency of background tasks.
- Kotlin Flow: Employed for reactive programming to handle data streams and UI updates efficiently.
- ConstraintLayout: Utilized for designing complex and responsive user interfaces.
- Navigation Components: Used for managing and simplifying the navigation flow within the app.
- Timber: Employed for logging purposes.
- CameraX Library: Utilized for capturing frames from the device's camera, which is crucial for determining lighting conditions and interacting with the Google ML Kit Face Detection.
- Google ML Kit Face Detection: Integrated for face recognition and detection, providing the capability to detect and analyze faces in camera frames and save relevant data for each frame.

Thank you!