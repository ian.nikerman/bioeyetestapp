package com.bioeye.bioeyetestapp.common.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.databinding.LayoutProgressBarBinding

/**
 * Custom progress bar view with loading animation.
 *
 * This view allows you to show and hide a loading animation based on the value of the "isProgress" flag.
 * It provides methods for handling the display of the loading animation.
 *
 * @property context The context in which the progress bar is used.
 * @property attrs An optional attribute set.
 * @property defStyleAttr An optional default style attribute.
 */

class ProgressBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: LayoutProgressBarBinding? = null

    /**
     * Initializes the progress bar by inflating its layout, setting up the binding, and configuring the animation.
     */
    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_progress_bar, this, true)
        binding = LayoutProgressBarBinding.bind(view)

        binding?.apply {
            // Hide the background view and set the loading animation
            lottieAnimationViewBkg.isVisible = false
            lottieAnimationView.setAnimation("loading.json")
        }
    }

    /**
     * Handles the display of the loading animation based on the specified flag.
     *
     * @param isProgress `true` to show the loading animation, `false` to hide it.
     */
    fun handleProgress(isProgress: Boolean) {
        when {
            isProgress -> binding?.showProgress()
            else -> binding?.hideProgress()
        }
    }

    /**
     * Shows the loading animation if it's currently hidden.
     */
    private fun LayoutProgressBarBinding.showProgress() {
        if (!lottieAnimationViewBkg.isVisible) {
            lottieAnimationViewBkg.isVisible = true
            lottieAnimationView.playAnimation()
        }
    }

    /**
     * Hides the loading animation if it's currently visible.
     */
    private fun LayoutProgressBarBinding.hideProgress() {
        if (lottieAnimationViewBkg.isVisible) {
            lottieAnimationViewBkg.isVisible = false
            lottieAnimationView.cancelAnimation()
        }
    }
}



