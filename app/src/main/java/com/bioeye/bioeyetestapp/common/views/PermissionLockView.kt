package com.bioeye.bioeyetestapp.common.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.databinding.LayoutPermissionLockBinding
import com.bioeye.bioeyetestapp.models.PermissionLockData
import com.bioeye.bioeyetestapp.models.PermissionStatus
import com.bioeye.bioeyetestapp.models.PermissionType

/**
 * Custom view for displaying a permission lock.
 *
 * This view allows you to show and hide a visual representation of a permission lock based on the status.
 * It provides methods for handling the display of the lock and setting permission-related data.
 *
 * @property context The context in which the PermissionLockView is used.
 * @property attrs An optional attribute set.
 * @property defStyleAttr An optional default style attribute.
 */
class PermissionLockView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: LayoutPermissionLockBinding? = null
    private var listener: PermissionLockViewListener? = null

    /**
     * Initializes the PermissionLockView by inflating its layout, setting up the binding, and configuring the visibility.
     */
    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_permission_lock, this, true)
        binding = LayoutPermissionLockBinding.bind(view)

        binding?.apply {

            // Set the initial visibility state of the lock
            permissionsLockView.isVisible = false

            // Set a click listener for the buttons
            submit.setListener(object : PrimaryButton.PrimaryButtonListener {
                override fun onButtonClick() {
                    listener?.onButtonClick()
                }
            })
        }
    }

    /**
     * Sets the status of the PermissionLockView based on the provided PermissionLockData.
     *
     * @param data The PermissionLockData containing information about the permission type and status.
     */
    fun setStatus(data: PermissionLockData) {
        binding?.setPermissionTitle(data.permissionType)
        when (data.permissionStatus) {
            PermissionStatus.GRANTED -> binding?.hidePermissionLock()
            PermissionStatus.DENIED -> binding?.showPermissionLock()
        }
    }

    private fun LayoutPermissionLockBinding.setPermissionTitle(permissionType: PermissionType) {
        when (permissionType) {
            PermissionType.CAMERA -> title.text = String.format(
                root.context.getString(R.string.screen_permissions_lock_title),
                root.context.getString(R.string.screen_permission_camera_lock_title)
            )
        }
    }

    /**
     * Shows the permission lock if it's currently hidden.
     */
    private fun LayoutPermissionLockBinding.showPermissionLock() {
        if (!permissionsLockView.isVisible) {
            permissionsLockView.isVisible = true
        }
    }

    /**
     * Hides the permission lock if it's currently visible.
     */
    private fun LayoutPermissionLockBinding.hidePermissionLock() {
        if (permissionsLockView.isVisible) {
            permissionsLockView.isVisible = false
        }
    }

    /**
     * Sets a listener for button clicks.
     *
     * @param listener The listener to be notified when the button is clicked.
     */
    fun setListener(listener: PermissionLockViewListener? = null) {
        this.listener = listener
    }

    /**
     * Interface for button click events.
     */
    interface PermissionLockViewListener {
        fun onButtonClick()
    }
}