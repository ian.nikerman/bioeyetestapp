package com.bioeye.bioeyetestapp.common.utils

import android.Manifest
import android.content.Context
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AlertDialog
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.common.utils.IntentUtils.getAppSettingsIntent

/**
 * Utility class containing extension functions for displaying dialogs related to permissions.
 */
object DialogUtils {

    /**
     * Displays a permission rationale dialog for the camera permission.
     *
     * @param context The context in which the dialog is displayed.
     * @param permissionRequest The ActivityResultLauncher for handling camera permission requests.
     */
    fun Context.showCameraRationaleDialog(
        permissionRequest: ActivityResultLauncher<String>
    ) {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.alert_dialog_permission_camera_message))
            .setPositiveButton(getString(R.string.alert_dialog_permission_positive_button)) { dialog, _ ->
                // Launch the camera permission request
                permissionRequest.launch(Manifest.permission.CAMERA)
                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.alert_dialog_permission_negative_button)) { dialog, _ ->
                // Dismiss the dialog
                dialog.dismiss()
            }
        builder.create().show()
    }

    /**
     * Displays a dialog guiding the user to the app settings to enable required permissions manually.
     *
     * @param context The context in which the dialog is displayed.
     */
    fun Context.showAppSettingsDialog() {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.alert_dialog_permission_required))
            .setMessage(getString(R.string.alert_dialog_app_settings_message))
            .setPositiveButton(getString(R.string.alert_dialog_permission_go_to_settings_button)) { _, _ ->
                // Open the app settings
                startActivity(getAppSettingsIntent(this))
            }
            .setNegativeButton(getString(R.string.alert_dialog_permission_cancel_button)) { dialog, _ ->
                // Dismiss the dialog
                dialog.dismiss()
            }
            .show()
    }
}