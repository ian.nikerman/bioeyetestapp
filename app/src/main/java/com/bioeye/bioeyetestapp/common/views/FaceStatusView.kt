package com.bioeye.bioeyetestapp.common.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.databinding.LayoutFaceStatusBinding
import com.bioeye.bioeyetestapp.models.FaceStatus

/**
 * Custom view for displaying face recognition status.
 *
 * This view is designed to display the recognition status of a face, such as whether it's valid or not.
 * It provides methods to set and update the view based on the face recognition status.
 *
 * @property context The context in which the view is used.
 * @property attrs An optional attribute set.
 * @property defStyleAttr An optional default style attribute.
 */

class FaceStatusView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: LayoutFaceStatusBinding? = null

    /**
     * Initializes the view by inflating its layout and setting up the binding.
     */
    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_face_status, this, true)
        binding = LayoutFaceStatusBinding.bind(view)
    }

    /**
     * Sets the face recognition status and updates the view accordingly.
     *
     * @param data The face recognition status to be displayed.
     */
    fun setFaceStatus(data: FaceStatus) {
        when (data) {
            FaceStatus.NOT_VALID -> binding?.showInvalidFaceStatusUi()
            FaceStatus.VALID -> binding?.showValidFaceStatusUi()
            else -> binding?.hideFaceStatus()
        }
    }

    /**
     * Updates the view to show an invalid face status.
     */
    private fun LayoutFaceStatusBinding.showInvalidFaceStatusUi() {
        statusTitle.isVisible = true
        statusTitle.text = context.getString(R.string.screen_recognition_face_not_detected)
        statusTitle.setTextAppearance(R.style.StatusNegativeTitleStyle)
        statusTitle.setCompoundDrawablesWithIntrinsicBounds(
            null,
            ContextCompat.getDrawable(context, R.drawable.ic_status_negative),
            null,
            null
        )
    }

    /**
     * Updates the view to show a valid face status.
     */
    private fun LayoutFaceStatusBinding.showValidFaceStatusUi() {
        statusTitle.isVisible = true
        statusTitle.text = context.getString(R.string.screen_recognition_face_detected)
        statusTitle.setTextAppearance(R.style.StatusPositiveTitleStyle)
        statusTitle.setCompoundDrawablesWithIntrinsicBounds(
            null,
            ContextCompat.getDrawable(context, R.drawable.ic_status_positive),
            null,
            null
        )
    }

    /**
     * Hides the face status view.
     */
    private fun LayoutFaceStatusBinding.hideFaceStatus() {
        statusTitle.isVisible = false
    }
}