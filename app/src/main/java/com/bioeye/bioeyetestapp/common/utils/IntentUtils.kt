package com.bioeye.bioeyetestapp.common.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.core.content.FileProvider
import com.bioeye.bioeyetestapp.common.Constants.INTENT_DATA_TYPE
import com.bioeye.bioeyetestapp.common.Constants.SCHEME_PACKAGE
import com.bioeye.bioeyetestapp.common.Constants.URI_FILE_PROVIDER
import java.io.File

/**
 * Utility class containing functions for creating Intents related to file sharing and app settings.
 */
object IntentUtils {

    /**
     * Creates an Intent for sharing a file using the specified URI.
     *
     * @param context The context in which the Intent is created.
     * @param file The file to be shared.
     * @return An Intent to share the specified file.
     */
    fun getShareFileIntent(context: Context, file: File): Intent {
        // Get the content URI for the file using FileProvider
        val uri = FileProvider.getUriForFile(context, context.packageName + URI_FILE_PROVIDER, file)

        // Create an Intent for sharing the file
        val intent = Intent(Intent.ACTION_SEND)
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        intent.setDataAndType(uri, INTENT_DATA_TYPE)
        intent.putExtra(Intent.EXTRA_STREAM, uri)
        return intent
    }

    /**
     * Creates an Intent to open the app settings for the current app.
     *
     * @param context The application context.
     * @return An Intent to open the app settings for the current app.
     */
    fun getAppSettingsIntent(context: Context): Intent {
        // Create an Intent to open the app settings
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)

        // Set the URI to identify the current app
        val uri = Uri.fromParts(SCHEME_PACKAGE, context.packageName, null)
        intent.data = uri
        return intent
    }
}
