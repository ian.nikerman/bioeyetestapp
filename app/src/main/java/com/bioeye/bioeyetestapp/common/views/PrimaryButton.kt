package com.bioeye.bioeyetestapp.common.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.databinding.LayoutPrimaryButtonBinding

/**
 * Custom button view for primary actions with optional text and icon.
 *
 * This view allows you to create primary buttons with text and an optional right arrow icon.
 * You can customize the button's appearance and behavior through attributes in XML and set a click listener.
 *
 * It has two types - Primary and Secondary.
 * Both types have two different layouts with distinct sets of parameters,
 * such as margin, stateListAnimator, and layout_width.
 *
 * @property context The context in which the button is used.
 * @property attrs An optional attribute set.
 * @property defStyleAttr An optional default style attribute.
 */

class PrimaryButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var binding: LayoutPrimaryButtonBinding? = null
    private var listener: PrimaryButtonListener? = null

    /**
     * Initializes the button by inflating its layout, setting up the binding, and handling attributes.
     */
    init {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_primary_button, this, true)
        binding = LayoutPrimaryButtonBinding.bind(view)

        binding?.apply {

            // Handle custom attributes defined in XML
            attrs.let {
                val attrArray = context.obtainStyledAttributes(attrs, R.styleable.PrimaryButton)
                if (attrArray.hasValue(R.styleable.PrimaryButton_withIcon)) {
                    val withIcon =
                        attrArray.getBoolean(R.styleable.PrimaryButton_withIcon, false)
                    setIconArrow(withIcon)
                }
                if (attrArray.hasValue(R.styleable.PrimaryButton_isSecondaryType)) {
                    val isSecondaryType =
                        attrArray.getBoolean(R.styleable.PrimaryButton_isSecondaryType, false)
                    checkSecondaryButtonType(isSecondaryType)
                }
                if (attrArray.hasValue(R.styleable.PrimaryButton_title)) {
                    val title = attrArray.getString(R.styleable.PrimaryButton_title)
                    setTitle(title)
                }
                attrArray.recycle()
            }

            // Set a click listener for the buttons
            primaryButton.setOnClickListener {
                listener?.onButtonClick()
            }
            secondaryButton.setOnClickListener {
                listener?.onButtonClick()
            }
        }
    }

    /**
     * Sets a listener for button clicks.
     *
     * @param listener The listener to be notified when the button is clicked.
     */
    fun setListener(listener: PrimaryButtonListener? = null) {
        this.listener = listener
    }

    /**
     * Interface for button click events.
     */
    interface PrimaryButtonListener {
        fun onButtonClick()
    }

    /**
     * Sets the title (text) of the button.
     *
     * @param title The text to be displayed on the button.
     */
    private fun LayoutPrimaryButtonBinding.setTitle(title: String?) {
        when {
            primaryButton.isVisible -> primaryButtonTitle.text = title
            secondaryButton.isVisible -> secondaryButtonTitle.text = title
        }
    }

    /**
     * Sets or removes the right arrow icon based on the specified flag.
     *
     * @param withIcon `true` to display the right arrow icon, `false` to hide it.
     */
    private fun LayoutPrimaryButtonBinding.setIconArrow(withIcon: Boolean) {
        if (withIcon) {
            primaryButtonTitle.setCompoundDrawablesWithIntrinsicBounds(
                null, null, ContextCompat.getDrawable(
                    primaryButtonTitle.context,
                    R.drawable.ic_arrow_right_black
                ), null
            )
        }
    }

    /**
     * Checks if the button is of secondary type and sets the button's visibility accordingly.
     *
     * If the button is of secondary type, it hides the primary button and shows the secondary button.
     * If not, it hides the secondary button and shows the primary button.
     *
     * @param isSecondaryType `true` if the button is of secondary type, `false` otherwise.
     */
    private fun LayoutPrimaryButtonBinding.checkSecondaryButtonType(isSecondaryType: Boolean) {
        primaryButton.isVisible = !isSecondaryType
        secondaryButton.isVisible = isSecondaryType
    }
}
