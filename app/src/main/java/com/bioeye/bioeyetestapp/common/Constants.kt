package com.bioeye.bioeyetestapp.common

/**
 * A collection of constant values used throughout the application.
 * These constants help maintain consistency and control various aspects of the app's behavior.
 * They include values for face detection status, lighting conditions, date and time format,
 * file name format, image analysis frequency, and maximum session duration.
 */
object Constants {

    // Constants for lux values indicating bright and dark lighting conditions
    const val LUX_MAX = 1000 // Maximum lux value for bright lighting conditions
    const val LUX_MIN = 20   // Minimum lux value for dark lighting conditions

    // Constants for controlling image analysis frequency and session duration
    const val SNAPSHOT_COUNT_MAX = 30 // Maximum number of image snapshots in a session
    const val RECOGNITION_DELAY_MS = 1000 // Delay in milliseconds for recognition analysis

    // Constants for face detection status
    const val VALUE_FACE_NOT_DETECTED = 0 // Value representing no face detection
    const val VALUE_FACE_DETECTED = 1    // Value representing face detection

    // Date and time format for timestamps in recognition data
    const val TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss" // Format for timestamps

    // Date and time format for generating unique file names
    const val FILENAME_TIMESTAMP_FORMAT = "yyyy-MM-dd_HH-mm-ss" // Format for file name timestamps

    const val FILENAME_PREFIX = "bioeye-" // Prefix for generated file names
    const val FILE_FORMAT = ".csv"        // File format for CSV files

    const val URI_FILE_PROVIDER = ".fileprovider" // FileProvider authority for URI
    const val INTENT_DATA_TYPE = "text/csv"        // MIME type for CSV files in Intent
    const val SCHEME_PACKAGE = "package"           // Scheme for identifying packages

}