package com.bioeye.bioeyetestapp.common.extensions

import com.bioeye.bioeyetestapp.common.Constants.LUX_MAX
import com.bioeye.bioeyetestapp.common.Constants.LUX_MIN
import com.bioeye.bioeyetestapp.common.Constants.VALUE_FACE_DETECTED
import com.bioeye.bioeyetestapp.common.Constants.VALUE_FACE_NOT_DETECTED
import com.bioeye.bioeyetestapp.models.FaceStatus
import com.bioeye.bioeyetestapp.models.LightingStatus
import com.bioeye.bioeyetestapp.models.PermissionStatus
import com.bioeye.bioeyetestapp.models.RecognitionData
import com.bioeye.bioeyetestapp.models.Summary

/**
 * Extension functions for common operations and calculations.
 */
object CommonExtensions {

    /**
     * Updates the lighting status based on the lux value obtained from the sensor data.
     *
     * @param lux The lux value obtained from the lighting sensor.
     * @return The updated [LightingStatus].
     */
    fun updateLightingData(lux: Int): LightingStatus {
        val status = when {
            lux < LUX_MIN -> LightingStatus.LIGHTING_DARK
            lux > LUX_MAX -> LightingStatus.LIGHTING_BRIGHT
            else -> LightingStatus.LIGHTING_OPTIMAL
        }
        return status
    }

    /**
     * Get the integer value of a FaceStatus.
     *
     * @param status The FaceStatus to convert to an integer.
     * @return An integer value representing the FaceStatus.
     */
    fun getStatusValue(status: FaceStatus): Int =
        if (status == FaceStatus.VALID) VALUE_FACE_DETECTED else VALUE_FACE_NOT_DETECTED

    /**
     * Generate a summary of recognition data.
     *
     * @param dataList The list of recognition data to summarize.
     * @return A [Summary] object containing the calculated summary values.
     */
    fun getSummary(dataList: List<RecognitionData>): Summary {
        return Summary(
            sessionTime = dataList.size,
            detectedTime = dataList.count { it.is_face_detected == VALUE_FACE_DETECTED },
            noFaceTime = dataList.count { it.is_face_detected == VALUE_FACE_NOT_DETECTED }
        )
    }

    /**
     * Maps a boolean value to a [PermissionStatus].
     *
     * @param granted The boolean value indicating whether a permission is granted.
     * @return The corresponding [PermissionStatus].
     */
    fun permissionStatus(granted: Boolean?): PermissionStatus =
        if (granted == true) PermissionStatus.GRANTED else PermissionStatus.DENIED

}