package com.bioeye.bioeyetestapp.common.dispatchers

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Interface for providing coroutine dispatchers for different execution contexts.
 */
interface DispatcherProvider {

    /**
     * Dispatcher for the main thread or UI thread.
     */
    val main: CoroutineDispatcher

    /**
     * Dispatcher for performing I/O operations, such as network or database operations.
     */
    val io: CoroutineDispatcher

    /**
     * Dispatcher for CPU-intensive work.
     */
    val default: CoroutineDispatcher

}

/**
 * Default implementation of [DispatcherProvider] using the [Dispatchers] object.
 */
class DefaultDispatcherProvider : DispatcherProvider {

    override val main: CoroutineDispatcher
        get() = Dispatchers.Main

    override val io: CoroutineDispatcher
        get() = Dispatchers.IO

    override val default: CoroutineDispatcher
        get() = Dispatchers.Default

}
