package com.bioeye.bioeyetestapp.di

import com.bioeye.bioeyetestapp.data.FileRepository
import com.bioeye.bioeyetestapp.data.RecognitionRepository
import com.bioeye.bioeyetestapp.data.SensorsRepository
import com.bioeye.bioeyetestapp.features.check.CheckViewModel
import com.bioeye.bioeyetestapp.features.main.MainViewModel
import com.bioeye.bioeyetestapp.features.recognition.RecognitionViewModel
import com.bioeye.bioeyetestapp.features.summary.SummaryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Koin module for providing application-wide dependencies.
 *
 * This module defines and configures dependencies used throughout the application, including
 * single instances for repositories such as RecognitionRepository and FileRepository.
 * Additionally, it provides a factory instance for SensorsRepository, which manages sensor-related
 * operations in the app. The module also includes view models associated with various app features,
 * enhancing dependency management within the app.
 */
val appModule = module {

    // Repository dependencies
    factory { SensorsRepository(get()) }
    single { RecognitionRepository() }
    single { FileRepository(get()) }

    // View model dependencies
    viewModel { MainViewModel() }
    viewModel { CheckViewModel(get(), get()) }
    viewModel { RecognitionViewModel(get(), get()) }
    viewModel { SummaryViewModel(get(), get(), get()) }
}