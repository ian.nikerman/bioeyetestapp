package com.bioeye.bioeyetestapp.di

import android.content.Context
import android.hardware.SensorManager
import com.bioeye.bioeyetestapp.common.dispatchers.DefaultDispatcherProvider
import com.bioeye.bioeyetestapp.common.dispatchers.DispatcherProvider
import org.koin.dsl.module
import java.io.File

/**
 * Koin module for providing essential services and dependencies.
 *
 * This module includes functions to provide the DispatcherProvider, SensorManager,
 * and a Cache Directory (File). These services are essential for coroutine dispatching,
 * sensor-related operations, and accessing the app's cache directory.
 */
val servicesModule = module {
    // Provide a single instance of the DispatcherProvider
    single { provideDispatcherProvider() }

    // Provide a single instance of the SensorManager
    single { provideSensorManager(get()) }

    // Provide a single instance of the Cache Directory
    single { provideCacheDirectory(get()) }
}

/**
 * Provides a DispatcherProvider with default dispatchers for various use cases.
 *
 * @return A DispatcherProvider instance.
 */
fun provideDispatcherProvider(): DispatcherProvider =
    DefaultDispatcherProvider()

/**
 * Provides a SensorManager based on the Android context.
 *
 * @param context The Android context used to access the SENSOR_SERVICE.
 * @return A SensorManager instance.
 */
fun provideSensorManager(context: Context): SensorManager =
    context.getSystemService(Context.SENSOR_SERVICE) as SensorManager

/**
 * Provides the app's cache directory based on the Android context.
 *
 * @param context The Android context used to access the app's cache directory.
 * @return The cache directory as a [File] instance.
 */
fun provideCacheDirectory(context: Context): File =
    context.cacheDir
