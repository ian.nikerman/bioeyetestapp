package com.bioeye.bioeyetestapp.data

import com.bioeye.bioeyetestapp.common.Constants.FILENAME_PREFIX
import com.bioeye.bioeyetestapp.common.Constants.FILENAME_TIMESTAMP_FORMAT
import com.bioeye.bioeyetestapp.common.Constants.FILE_FORMAT
import com.bioeye.bioeyetestapp.models.RecognitionData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * `FileRepository` is responsible for creating and writing CSV files to external storage.
 *
 * @param cacheDirectory The cache directory where CSV files are stored.
 */
class FileRepository(
    private val cacheDirectory: File
) {

    /**
     * Writes a CSV file to external storage.
     *
     * @param dataList The list of `RecognitionData` to be written to the CSV file.
     * @return A [Flow] emitting the [File] reference of the created CSV file.
     */
    fun writeCSVFile(dataList: List<RecognitionData>): Flow<File?> = flow {
        Timber.tag(TAG).d(::writeCSVFile.name)
        val result = writeDataToFile(dataList)
        emit(result)
    }

    /**
     * Writes [RecognitionData] to a CSV file.
     *
     * @param dataList The list of `RecognitionData` to be written to the CSV file.
     * @return The [File] reference of the created CSV file, or `null` if an error occurs.
     */
    private fun writeDataToFile(dataList: List<RecognitionData>): File? {
        Timber.tag(TAG).d(::writeDataToFile.name)

        // Clean cached files in directory
        cleanCache()

        val file = createFileInCacheDirectory(getCSVFileName())

        return try {
            // Writing data to the CSV file
            val os: OutputStream = FileOutputStream(file)
            os.write("timestamp,is_face_detected\n".toByteArray())

            for (data in dataList) {
                val line = "${data.timestamp},${data.is_face_detected}\n"
                os.write(line.toByteArray())
            }
            os.close()

            file
        } catch (e: Exception) {
            Timber.tag(TAG).e("${::writeDataToFile.name} failed: ${e.message}")
            null
        }
    }

    /**
     * Creates a file in the cache directory with the specified file name.
     *
     * @param fileName The name of the file to be created.
     * @return The [File] reference of the created file.
     */
    private fun createFileInCacheDirectory(fileName: String): File {
        Timber.tag(TAG).d(::createFileInCacheDirectory.name)
        
        val file = File(cacheDirectory, fileName)

        if (!file.exists()) {
            file.createNewFile()
        }

        return file
    }

    /**
     * Generates a unique CSV file name based on the current timestamp.
     *
     * @return A unique CSV file name.
     */
    private fun getCSVFileName(): String {
        val timeStamp =
            SimpleDateFormat(FILENAME_TIMESTAMP_FORMAT, Locale.getDefault()).format(Date())
        return "$FILENAME_PREFIX$timeStamp$FILE_FORMAT"
    }

    /**
     * Cleans the cache directory by deleting all files and empty directories within it.
     * This function provides a way to free up storage space by removing temporary files.
     */
    private fun cleanCache() {
        Timber.tag(TAG).d(::cleanCache.name)
        try {
            // Delete all files and directories in the cache directory
            deleteFilesInDir(cacheDirectory)
        } catch (e: Exception) {
            Timber.tag(TAG).e("${::cleanCache.name} failed: ${e.message}")
        }
    }

    /**
     * Recursively deletes files and empty directories within the specified directory.
     *
     * @param dir The directory to clean by deleting its contents.
     * @return `true` if the operation is successful, `false` otherwise.
     */
    private fun deleteFilesInDir(dir: File): Boolean {
        return try {
            val children = dir.listFiles()
            if (children != null) {
                for (child in children) {
                    if (child.isDirectory) {
                        // Recursively delete files and directories
                        deleteFilesInDir(child)
                    }
                    // Delete files and empty directories
                    child.delete()
                }
            }
            true
        } catch (e: Exception) {
            Timber.tag(TAG).e("${::deleteFilesInDir.name} failed: ${e.message}")
            false
        }
    }

    companion object {
        private val TAG = FileRepository::class.java.simpleName
    }
}