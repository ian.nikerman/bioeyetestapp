package com.bioeye.bioeyetestapp.data

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import timber.log.Timber

/**
 * Repository for accessing sensor data and providing updates as a Flow.
 *
 * This repository allows you to subscribe to sensor updates as a Flow of Float values, specifically for the light sensor.
 *
 * @property sensorManager The SensorManager used to access sensor data.
 */
class SensorsRepository(
    private val sensorManager: SensorManager
) {

    /**
     * Retrieves and shares light sensor data updates as a Flow of Float values.
     *
     * This function creates a callbackFlow to listen for sensor changes and provides updates as a Flow.
     *
     * @return A Flow of Float values representing light sensor data.
     */
    suspend fun getSensorsUpdates(): Flow<Float> = callbackFlow {
        val callback: SensorEventListener = object : SensorEventListener {
            override fun onSensorChanged(event: SensorEvent) {
                if (event.sensor.type == Sensor.TYPE_LIGHT) {
                    trySend(event.values[0])
                }
            }

            override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {
                // Ignoring accuracy changes for the light sensor.
            }
        }

        val lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)
        if (lightSensor != null) {
            sensorManager.registerListener(
                callback, lightSensor, SensorManager.SENSOR_DELAY_NORMAL
            )
            Timber.tag(TAG).d("${::sensorManager.name} : Start listening for the sensor updates.")

            awaitClose {
                sensorManager.unregisterListener(callback)
                Timber.tag(TAG).d("${::sensorManager.name} : Stopped listening for the sensor updates.")
            }
        } else {
            close()
            Timber.tag(TAG).e("${::sensorManager.name} : Light sensor is not available.")
        }
    }

    companion object {
        private val TAG = SensorsRepository::class.java.simpleName
    }
}
