package com.bioeye.bioeyetestapp.data

import com.bioeye.bioeyetestapp.common.Constants.TIMESTAMP_FORMAT
import com.bioeye.bioeyetestapp.common.extensions.CommonExtensions.getStatusValue
import com.bioeye.bioeyetestapp.models.FaceStatus
import com.bioeye.bioeyetestapp.models.RecognitionData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Repository for managing face recognition sessions.
 *
 * This repository handles the face recognition process and session data management.
 */
class RecognitionRepository {

    /**
     * Start face recognition and provide updates as a Flow of FaceStatus.
     *
     * This function initiates the face recognition process and provides recognition status updates as a Flow.
     *
     * @param status The initial face status to start the recognition process with.
     * @return A Flow of FaceStatus updates.
     */
    suspend fun handleFaceStatus(status: FaceStatus): Flow<FaceStatus> {
        Timber.tag(TAG).d("${::handleFaceStatus.name} : Success, initial faceStatus: $status")
        return flow {
            if (status != FaceStatus.EMPTY) {
                saveStateToList(status)
            }
            emit(status)
        }
    }

    /**
     * Fetches session data as a Flow of a copy of the List of RecognitionData objects.
     *
     * @return A Flow containing a copy of the List of RecognitionData objects representing the session data.
     */
    suspend fun fetchSessionData(): Flow<List<RecognitionData>> {
        Timber.tag(TAG).d(::fetchSessionData.name)
        return flow {
            emit(recognitionList.toList()) // Emitting a copy to ensure immutability
        }
    }

    /**
     * Saves the face recognition status to a list along with a timestamp.
     *
     * @param faceStatus The current face recognition status.
     */
    private fun saveStateToList(faceStatus: FaceStatus) {
        Timber.tag(TAG).d(::saveStateToList.name)
        val timeStamp = SimpleDateFormat(TIMESTAMP_FORMAT, Locale.getDefault()).format(Date())
        recognitionList.add(
            RecognitionData(
                timeStamp,
                getStatusValue(faceStatus)
            )
        )
    }

    /**
     * Resets the session by clearing the recognition list.
     */
    fun resetSession() {
        Timber.tag(TAG).d(::resetSession.name)
        recognitionList.clear()
    }

    companion object {
        private val TAG = RecognitionRepository::class.java.simpleName

        // List to store recognition data during a session
        private val recognitionList = mutableListOf<RecognitionData>()
    }
}