package com.bioeye.bioeyetestapp.features.recognition

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bioeye.bioeyetestapp.common.dispatchers.DispatcherProvider
import com.bioeye.bioeyetestapp.common.extensions.CommonExtensions.permissionStatus
import com.bioeye.bioeyetestapp.data.RecognitionRepository
import com.bioeye.bioeyetestapp.models.FaceStatus
import com.bioeye.bioeyetestapp.models.PermissionLockData
import com.bioeye.bioeyetestapp.models.PermissionStatus
import com.bioeye.bioeyetestapp.models.PermissionType
import com.bioeye.bioeyetestapp.models.ResponseState
import com.bioeye.bioeyetestapp.models.Status
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * ViewModel class responsible for managing and providing face recognition data.
 *
 * This ViewModel serves as the central component for orchestrating face recognition sessions
 * and updating the UI with relevant data. It communicates with the underlying data repositories and
 * processes recognition data.
 *
 * @property dispatcherProvider The provider for dispatchers for performing background operations.
 * @property sessionRepository The repository for handling face recognition sessions and saving session data summaries.
 */
class RecognitionViewModel(
    private val dispatcherProvider: DispatcherProvider,
    private val sessionRepository: RecognitionRepository
) : ViewModel() {

    // Mutable state flow for permission data
    private val _permissionState = initPermissionState()

    // Public shared flow to observe permission data
    val permissionState = _permissionState.asSharedFlow()

    // Mutable state flow for face detection data
    private val _faceDetectionData = initFaceDetectionData()

    // Public shared flow to observe face detection data
    val faceDetectionData = _faceDetectionData.asSharedFlow()

    /**
     * Initiates a face recognition session with the provided initial face status.
     *
     * This method starts a face recognition session and sets the initial face status for the session.
     *
     * @param status The initial face status to start the session with.
     */
    fun handleFaceStatus(status: FaceStatus) {
        Timber.tag(TAG).d("${::handleFaceStatus.name} : Initiating face recognition session.")

        viewModelScope.launch {
            _faceDetectionData.value = ResponseState.loading()
            // Reset the session data to start fresh
            sessionRepository.handleFaceStatus(status)
                .flowOn(dispatcherProvider.default)
                .catch { handleError(it) }
                .collect { _faceDetectionData.value = ResponseState.success(it) }
        }
    }

    /**
     * Ends the face recognition session manually.
     *
     * This method manually ends a face recognition session, saving any collected data.
     */
    fun endSessionManually() {
        Timber.tag(TAG)
            .d("${::endSessionManually.name} : Manually ending face recognition session.")
        handleFaceStatus(FaceStatus.EMPTY)
    }

    /**
     * Handles exceptions and updates the state with an error status.
     *
     * This method handles exceptions that may occur during the session and
     * updates the state with an error status containing the error message.
     *
     * @param exception The exception that occurred during session data retrieval.
     */
    private fun handleError(exception: Throwable) {
        Timber.tag(TAG).e("${::handleError.name} failed: ${exception.message.toString()}")
        _faceDetectionData.value = ResponseState.error(exception.message.toString())
    }

    /**
     * Resets the face recognition session.
     */
    fun resetSession() {
        Timber.tag(TAG).d("${::resetSession.name} : Resetting face recognition session.")
        sessionRepository.resetSession()
    }

    /**
     * Handles the permission state update and sets the corresponding data in the state flow.
     *
     * @param granted True if the permission is granted, false otherwise.
     */
    fun handlePermissionState(granted: Boolean?) {
        Timber.tag(TAG).d("${::handlePermissionState.name} : granted : $granted")
        _permissionState.value = PermissionLockData(
            permissionType = PermissionType.CAMERA,
            permissionStatus = permissionStatus(granted)
        )
    }

    companion object {
        private val TAG = RecognitionViewModel::class.java.simpleName

        /**
         * Initializes the mutable state flow for face detection data with an initial empty state.
         */
        private fun initFaceDetectionData() = MutableStateFlow(
            ResponseState(
                Status.LOADING,
                FaceStatus.EMPTY,
                ""
            )
        )

        /**
         * Initializes the mutable state flow for permission data with an initial denied state.
         */
        private fun initPermissionState() =
            MutableStateFlow(
                PermissionLockData(
                    permissionType = PermissionType.CAMERA,
                    permissionStatus = PermissionStatus.DENIED
                )
            )
    }
}