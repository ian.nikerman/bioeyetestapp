package com.bioeye.bioeyetestapp.features.recognition

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bioeye.bioeyetestapp.common.utils.DialogUtils.showAppSettingsDialog
import com.bioeye.bioeyetestapp.common.utils.DialogUtils.showCameraRationaleDialog
import com.bioeye.bioeyetestapp.common.views.PermissionLockView
import com.bioeye.bioeyetestapp.common.views.PrimaryButton
import com.bioeye.bioeyetestapp.databinding.FragmentRecognitionBinding
import com.bioeye.bioeyetestapp.features.main.MainViewModel
import com.bioeye.bioeyetestapp.helpers.camerax.FaceRecognitionProviderClient
import com.bioeye.bioeyetestapp.models.FaceStatus
import com.bioeye.bioeyetestapp.models.PermissionLockData
import com.bioeye.bioeyetestapp.models.PermissionStatus
import com.bioeye.bioeyetestapp.models.Status
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * Fragment responsible for the face recognition process.
 */
class RecognitionFragment : Fragment() {

    // SharedViewModel associated with the MainActivity
    private val sharedViewModel by activityViewModel<MainViewModel>()

    // ViewModel specific to the RecognitionFragment
    private val viewModel: RecognitionViewModel by viewModel()

    // View binding instance for the fragment
    private var _binding: FragmentRecognitionBinding? = null
    private val binding get() = _binding!!

    // Face recognition client for managing recognition updates
    private var faceRecognitionProviderClient: FaceRecognitionProviderClient? = null

    // Callback to handle face recognition state changes
    private lateinit var faceRecognitionCallback: FaceRecognitionProviderClient.FaceRecognitionCallback

    // Permission result callback for camera permissions
    private val permissionCameraRequest =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            viewModel.handlePermissionState(granted)
        }

    /**
     * Inflates the view for this fragment.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecognitionBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Resumes the fragment, checking permissions before user interaction.
     */
    override fun onResume() {
        super.onResume()
        checkPermissions()
    }

    /**
     * Pauses the fragment, stopping the face recognition process.
     */
    override fun onPause() {
        stopFaceRecognition()
        super.onPause()
    }

    /**
     * Cleans up resources when the view is destroyed.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Initializes the UI components and sets up observers.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initObservers()
    }

    /**
     * Initializes the UI components.
     */
    private fun initUI() {
        binding.apply {
            permissionLockView.setListener(object : PermissionLockView.PermissionLockViewListener {
                override fun onButtonClick() {
                    requireActivity().showAppSettingsDialog()
                }
            })

            submit.setListener(object : PrimaryButton.PrimaryButtonListener {
                override fun onButtonClick() {
                    stopFaceRecognition()
                    viewModel.endSessionManually()
                }
            })
        }
    }

    /**
     * Initializes observers for observing permission and face detection data changes.
     */
    private fun initObservers() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.permissionState.collect {
                        handlePermissionState(it)
                    }
                }

                launch {
                    viewModel.faceDetectionData.collect { uiState ->
                        when (uiState.status) {
                            Status.LOADING -> showProgress(true)
                            Status.SUCCESS -> {
                                showProgress(false)
                                handleFaceStatus(uiState.data)
                            }

                            Status.ERROR -> {
                                showProgress(false)
                                showError(uiState.message)
                            }

                            else -> { /* Do nothing for other states */
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Handles the permission state and updates the UI accordingly.
     */
    private fun handlePermissionState(data: PermissionLockData) {
        binding.permissionLockView.setStatus(data)
        when (data.permissionStatus) {
            PermissionStatus.GRANTED -> startFaceRecognition()
            PermissionStatus.DENIED -> stopFaceRecognition()
        }
    }

    /**
     * Handles the face status and updates the UI accordingly.
     */
    private fun handleFaceStatus(faceStatus: FaceStatus?) {
        faceStatus?.let {
            binding.faceStatusView.setFaceStatus(it)

            if (faceStatus == FaceStatus.EMPTY) {
                sharedViewModel.openSummary()
            }
        }
    }

    /**
     * Shows or hides the progress bar based on the loading state.
     */
    private fun showProgress(isProgress: Boolean) {
        binding.progressBar.handleProgress(isProgress)
    }

    /**
     * Shows an error message using a Toast.
     */
    private fun showError(message: String?) {
        Toast.makeText(
            requireContext(), message, Toast.LENGTH_LONG
        ).show()
    }

    /**
     * Check and request necessary permissions for the face recognition process.
     *
     * This function checks if the app has the required permissions for camera and storage access.
     * It handles different scenarios, including showing permission rationale dialogs, guiding users to app settings,
     * and launching permission requests. If all required permissions are granted, it initiates the face recognition process.
     */
    private fun checkPermissions() {
        when {
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) ->
                requireContext().showCameraRationaleDialog(permissionCameraRequest)

            else -> permissionCameraRequest.launch(Manifest.permission.CAMERA)
        }
    }

    /**
     * Starts the face recognition process.
     *
     * This function initializes the face recognition process by resetting the session data,
     * creating a callback to handle recognition state changes, and requesting recognition updates
     * from the `FaceRecognitionProviderClient`. It also stops the face recognition process if the
     * recognition status becomes empty.
     */
    private fun startFaceRecognition() {
        Timber.tag(TAG).d(::startFaceRecognition.name)

        // Reset the session data to start fresh
        viewModel.resetSession()

        // Callback to handle recognition state changes
        faceRecognitionCallback = object : FaceRecognitionProviderClient.FaceRecognitionCallback {
            override fun onChangeState(status: FaceStatus) {
                if (status == FaceStatus.EMPTY) {
                    // Stop the face recognition process if recognition status is empty
                    stopFaceRecognition()
                }
                viewModel.handleFaceStatus(status)
            }

            override fun onCameraFailed(message: String) {
                showError(message)
            }
        }

        // Initialize the FaceRecognitionProviderClient and request recognition updates
        faceRecognitionProviderClient = FaceRecognitionProviderClient().apply {
            requestRecognitionUpdates(
                ContextCompat.getMainExecutor(requireContext()),
                ProcessCameraProvider.getInstance(requireContext()),
                binding.cameraView,
                this@RecognitionFragment,
                faceRecognitionCallback
            )
        }
    }

    /**
     * Stops the face recognition process.
     *
     * This function stops the face recognition process by removing recognition updates if the
     * recognition callback has been initialized.
     */
    private fun stopFaceRecognition() {
        Timber.tag(TAG).d(::stopFaceRecognition.name)

        // Check if the recognition callback is initialized and remove recognition updates
        if (::faceRecognitionCallback.isInitialized) {
            faceRecognitionProviderClient?.removeRecognitionUpdates()
        }
    }

    companion object {
        private val TAG = RecognitionFragment::class.java.simpleName
    }
}