package com.bioeye.bioeyetestapp.features.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.databinding.ActivityMainBinding
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * The main activity of the BioEyeTestApp.
 *
 * This activity serves as the entry point for the application and hosts the navigation container.
 * It observes the navigation events from the associated ViewModel and handles navigation accordingly.
 */
class MainActivity : AppCompatActivity() {

    // ViewModel associated with the MainActivity
    private val viewModel: MainViewModel by viewModel()

    // View binding instance for the fragment
    private lateinit var binding: ActivityMainBinding

    /**
     * Called when the activity is first created.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initObservers()
    }

    /**
     * Initializes observers for handling navigation events from the ViewModel.
     */
    private fun initObservers() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.navigationEvent.collect {
                        handleNavigationEvent(it)
                    }
                }
            }
        }
    }

    /**
     * Handles a navigation event by navigating to the specified destination.
     *
     * @param navDirections The directions to navigate to.
     */
    private fun handleNavigationEvent(navDirections: NavDirections?) {
        navDirections?.let { dir ->
            findNavController(R.id.navigationContainer).navigate(dir)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        // Handle Up navigation by delegating to the NavController
        val navController = findNavController(R.id.navigationContainer)
        return navController.navigateUp()
                || super.onSupportNavigateUp()
    }
}