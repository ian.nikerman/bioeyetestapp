package com.bioeye.bioeyetestapp.features.summary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bioeye.bioeyetestapp.common.dispatchers.DispatcherProvider
import com.bioeye.bioeyetestapp.common.extensions.CommonExtensions.getSummary
import com.bioeye.bioeyetestapp.data.FileRepository
import com.bioeye.bioeyetestapp.data.RecognitionRepository
import com.bioeye.bioeyetestapp.models.RecognitionData
import com.bioeye.bioeyetestapp.models.ResponseState
import com.bioeye.bioeyetestapp.models.ShareFileData
import com.bioeye.bioeyetestapp.models.Status
import com.bioeye.bioeyetestapp.models.Summary
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File

/**
 * ViewModel class responsible for managing and providing summary data.
 *
 * This ViewModel class is responsible for fetching and managing session summary data.
 *
 * @property dispatcherProvider The dispatcher provider for coroutine context.
 * @property recognitionRepository The repository to fetch session summary data.
 * @property fileRepository The repository to write session summary data to a CSV file.
 */
class SummaryViewModel(
    private val dispatcherProvider: DispatcherProvider,
    private val recognitionRepository: RecognitionRepository,
    private val fileRepository: FileRepository
) : ViewModel() {

    // Mutable state flow for summary data
    private val _summaryData = initSummaryData()

    // Public shared flow to observe summary data
    val summaryData = _summaryData.asSharedFlow()

    // Mutable state flow for share file data
    private val _shareFileData = initShareFileData()

    // Public shared flow to observe share file data
    val shareFileData = _shareFileData.asSharedFlow()

    init {
        handleSummary()
    }

    /**
     * Initiates the process to retrieve and handle session summary data.
     */
    private fun handleSummary() {
        Timber.tag(TAG).d(::handleSummary.name)

        viewModelScope.launch {
            _summaryData.value = ResponseState.loading()

            // Fetch session summary data and handle exceptions
            recognitionRepository.fetchSessionData()
                .flowOn(dispatcherProvider.default)
                .catch { handleError(it) }
                .collect {
                    handleSessionData(it)
                    saveSessionDataToFile(it)
                }
        }
    }

    /**
     * Saves session summary data to a CSV file.
     *
     * @param dataList The session summary data to be saved.
     */
    private fun saveSessionDataToFile(dataList: List<RecognitionData>) {
        Timber.tag(TAG).d(::saveSessionDataToFile.name)

        // Write a CSV file with the provided data list
        viewModelScope.launch {
            fileRepository.writeCSVFile(dataList)
                .flowOn(dispatcherProvider.io)
                .catch { handleError(it) }
                .collect { handleFileData(it) }
        }
    }

    /**
     * Handles exceptions and updates the state with an error status.
     *
     * @param exception The exception that occurred during data retrieval.
     */
    private fun handleError(exception: Throwable) {
        Timber.tag(TAG).e("${::handleError.name} failed: ${exception.message.toString()}")
        _summaryData.value = ResponseState.error(exception.message.toString())
    }

    /**
     * Handles the session summary data received from the repository.
     *
     * @param summary The session summary data.
     */
    private fun handleSessionData(dataList: List<RecognitionData>) {
        Timber.tag(TAG).d(::handleSessionData.name)
        _summaryData.value = ResponseState.success(getSummary(dataList))
    }

    /**
     * Handles the file for share intent.
     *
     * @param file The CSV file to be shared.
     */
    private fun handleFileData(file: File?) {
        file?.let {
            Timber.tag(TAG).d("${::handleFileData.name} - File: $it")
            _shareFileData.value = ShareFileData(it)
        }
    }

    companion object {
        private val TAG = SummaryViewModel::class.java.simpleName

        /**
         * Initializes the mutable state flow for summary data with an initial empty state.
         */
        private fun initSummaryData() = MutableStateFlow(
            ResponseState(
                Status.EMPTY,
                Summary(
                    sessionTime = 0,
                    detectedTime = 0,
                    noFaceTime = 0
                ),
                ""
            )
        )

        /**
         * Initializes the mutable state flow for share file data.
         */
        private fun initShareFileData() = MutableStateFlow(
            ShareFileData(null)
        )
    }
}