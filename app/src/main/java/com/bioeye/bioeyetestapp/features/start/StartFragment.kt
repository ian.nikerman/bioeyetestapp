package com.bioeye.bioeyetestapp.features.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bioeye.bioeyetestapp.common.views.PrimaryButton
import com.bioeye.bioeyetestapp.databinding.FragmentStartBinding
import com.bioeye.bioeyetestapp.features.main.MainViewModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel

/**
 * Fragment responsible for the initial screen of the app.
 * Users can start the eye test by tapping the primary button.
 *
 * This fragment serves as the starting point for the app. Users can initiate the eye test
 * by tapping the primary button, which navigates them to the Lighting Check screen.
 */
class StartFragment : Fragment() {

    // SharedViewModel associated with the MainActivity
    private val sharedViewModel by activityViewModel<MainViewModel>()

    // View binding instance for the fragment
    private var _binding: FragmentStartBinding? = null
    private val binding get() = _binding!!

    /**
     * Inflates the view for this fragment.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Cleans up resources when the view is destroyed.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Initializes the UI components.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    /**
     * Sets up UI components and their interactions.
     */
    private fun initUI() {
        binding.submit.setListener(object : PrimaryButton.PrimaryButtonListener {
            override fun onButtonClick() {
                // Navigate to the Lighting Check screen when the button is clicked
                sharedViewModel.openLightingCheck()
            }
        })
    }
}