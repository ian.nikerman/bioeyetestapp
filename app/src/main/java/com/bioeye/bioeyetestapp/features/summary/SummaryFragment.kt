package com.bioeye.bioeyetestapp.features.summary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.common.utils.IntentUtils.getShareFileIntent
import com.bioeye.bioeyetestapp.common.views.PrimaryButton
import com.bioeye.bioeyetestapp.databinding.FragmentSummaryBinding
import com.bioeye.bioeyetestapp.models.ShareFileData
import com.bioeye.bioeyetestapp.models.Status
import com.bioeye.bioeyetestapp.models.Summary
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Fragment responsible for displaying the summary of the face recognition process.
 */
class SummaryFragment : Fragment() {

    // ViewModel specific to the SummaryFragment
    private val viewModel: SummaryViewModel by viewModel()

    // View binding instance for the fragment
    private var _binding: FragmentSummaryBinding? = null
    private val binding get() = _binding!!

    /**
     * Inflates the view for this fragment.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSummaryBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Cleans up resources when the view is destroyed.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Initializes the UI components and sets up observers.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initObservers()
    }

    /**
     * Initializes the UI components.
     */
    private fun initUI() {
        binding.apply {

            submit.setListener(object : PrimaryButton.PrimaryButtonListener {
                override fun onButtonClick() {
                    requireActivity().onBackPressedDispatcher.onBackPressed()
                }
            })
        }
    }

    /**
     * Sets up observers for observing ViewModel data changes.
     */
    private fun initObservers() {
        // Observe ViewModel data using lifecycleScope
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.summaryData.collect { uiState ->
                        when (uiState.status) {
                            Status.LOADING -> handleProgressBar(true)
                            Status.SUCCESS -> {
                                handleProgressBar(false)
                                handleSummaryData(uiState.data)
                            }

                            Status.ERROR -> {
                                handleProgressBar(false)
                                showError(uiState.message)
                            }

                            else -> {}
                        }
                    }
                }
                launch {
                    viewModel.shareFileData.collect {
                        handleShareData(it)
                    }
                }
            }
        }
    }

    /**
     * Handles summary data changes and updates the UI accordingly.
     */
    private fun handleSummaryData(summary: Summary?) {
        summary?.let { data ->
            binding.apply {
                sessionTime.text =
                    String.format(
                        getString(R.string.screen_summary_duration_title),
                        data.sessionTime
                    )
                detectedTime.text = String.format(
                    getString(R.string.screen_summary_face_detected_title),
                    data.detectedTime
                )
                noFaceTime.text =
                    String.format(getString(R.string.screen_summary_no_face_title), data.noFaceTime)
            }
        }
    }

    /**
     * Handles share data changes and updates the UI accordingly.
     */
    private fun handleShareData(data: ShareFileData?) {
        data?.file?.let { file ->
            binding.apply {
                shareButton.isVisible = true
                shareButton.setOnClickListener {
                    startActivity(
                        getShareFileIntent(
                            requireContext(),
                            file
                        )
                    )
                }
            }
        }
    }

    /**
     * Shows or hides the progress bar based on the loading state.
     */
    private fun handleProgressBar(isProgress: Boolean) {
        binding.progressBar.handleProgress(isProgress)
    }

    /**
     * Shows an error message using a Toast.
     */
    private fun showError(message: String?) {
        Toast.makeText(
            requireContext(), message, Toast.LENGTH_LONG
        ).show()
    }
}