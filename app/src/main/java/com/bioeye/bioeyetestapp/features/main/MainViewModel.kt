package com.bioeye.bioeyetestapp.features.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.bioeye.bioeyetestapp.features.check.CheckFragmentDirections
import com.bioeye.bioeyetestapp.features.recognition.RecognitionFragmentDirections
import com.bioeye.bioeyetestapp.features.start.StartFragmentDirections
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * ViewModel class responsible for handling navigation events and routing between fragments.
 */
class MainViewModel : ViewModel() {

    // Mutable shared flow for navigation events
    private val _navigationEvent = initNavigationEvent()

    // Public shared flow to observe navigation events
    val navigationEvent = _navigationEvent.asSharedFlow()

    /**
     * Opens the lighting check fragment by emitting the corresponding navigation event.
     */
    fun openLightingCheck() {
        Timber.tag(TAG).d(::openLightingCheck.name)

        // Launch a coroutine to emit the navigation event
        viewModelScope.launch {
            _navigationEvent.emit(
                StartFragmentDirections.actionStartToCheck()
            )
        }
    }

    /**
     * Opens the face recognition fragment by emitting the corresponding navigation event.
     */
    fun openFaceRecognition() {
        Timber.tag(TAG).d(::openFaceRecognition.name)

        // Launch a coroutine to emit the navigation event
        viewModelScope.launch {
            _navigationEvent.emit(CheckFragmentDirections.actionCheckToRecognition())
        }
    }

    /**
     * Opens the summary fragment by emitting the corresponding navigation event.
     */
    fun openSummary() {
        Timber.tag(TAG).d(::openSummary.name)

        // Launch a coroutine to emit the navigation event
        viewModelScope.launch {
            _navigationEvent.emit(
                RecognitionFragmentDirections.actionRecognitionToSummary()
            )
        }
    }

    companion object {
        private val TAG = MainViewModel::class.java.simpleName

        // Initializes the navigation event flow with an initial null value
        private fun initNavigationEvent() = MutableSharedFlow<NavDirections?>()
    }
}