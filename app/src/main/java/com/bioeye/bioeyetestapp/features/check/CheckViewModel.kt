package com.bioeye.bioeyetestapp.features.check

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bioeye.bioeyetestapp.common.dispatchers.DispatcherProvider
import com.bioeye.bioeyetestapp.common.extensions.CommonExtensions.updateLightingData
import com.bioeye.bioeyetestapp.data.SensorsRepository
import com.bioeye.bioeyetestapp.models.LightingStatus
import com.bioeye.bioeyetestapp.models.ResponseState
import com.bioeye.bioeyetestapp.models.Status
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * ViewModel class responsible for checking lighting conditions using the lighting sensor.
 *
 * @property dispatcherProvider The provider for coroutine dispatchers.
 * @property sensorsRepository The repository to access sensor data.
 */
class CheckViewModel(
    private val dispatcherProvider: DispatcherProvider,
    private val sensorsRepository: SensorsRepository
) : ViewModel() {

    // Mutable state flow for lighting data
    private val _lightingData = initLightingData()

    // Public shared flow to observe lighting data
    val lightingData = _lightingData.asSharedFlow()

    init {
        checkLighting()
    }

    /**
     * Initiates the lighting check by fetching data from the lighting sensor.
     */
    fun checkLighting() {
        Timber.tag(TAG).d("${::checkLighting.name} : Initiating lighting check.")

        viewModelScope.launch {
            _lightingData.value = ResponseState.loading()

            // Fetch lighting sensor data and handle exceptions
            sensorsRepository.getSensorsUpdates()
                .flowOn(dispatcherProvider.default)
                .catch { handleError(it) }
                .collect { handleLightingData(it.toInt()) }
        }
    }

    /**
     * Handles the lighting data received from the sensor.
     *
     * @param lux The lux value obtained from the lighting sensor.
     */
    private fun handleLightingData(lux: Int) {
        Timber.tag(TAG).d("${::handleLightingData.name} : Handling lighting data.")

        // Update lighting data status
        val status = updateLightingData(lux)
        updateState(status)
    }

    /**
     * Updates the state with the provided lighting status.
     *
     * @param status The lighting status to set in the state.
     */
    private fun updateState(status: LightingStatus) {
        Timber.tag(TAG).d("${::updateState.name} : Updating state with lighting status.")

        // Update the state with success status and lighting data
        _lightingData.value = ResponseState.success(status)
    }

    /**
     * Handles exceptions and updates the state with an error status.
     *
     * @param exception The exception that occurred during sensor data retrieval.
     */
    private fun handleError(exception: Throwable) {
        Timber.tag(TAG).e("${::checkLighting.name} : Error occurred - ${exception.message}")

        // Update the state with an error status
        _lightingData.value = ResponseState.error(exception.message.toString())
    }

    companion object {
        private val TAG = CheckViewModel::class.java.simpleName

        /**
         * Initializes the mutable state flow for lighting data with an initial empty state.
         */
        private fun initLightingData() = MutableStateFlow(
            ResponseState(
                Status.EMPTY,
                LightingStatus.LIGHTING_CHECK,
                ""
            )
        )
    }
}