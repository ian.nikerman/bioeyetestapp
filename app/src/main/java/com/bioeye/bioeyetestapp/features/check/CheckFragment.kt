package com.bioeye.bioeyetestapp.features.check

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bioeye.bioeyetestapp.R
import com.bioeye.bioeyetestapp.common.views.PrimaryButton
import com.bioeye.bioeyetestapp.databinding.FragmentCheckBinding
import com.bioeye.bioeyetestapp.features.main.MainViewModel
import com.bioeye.bioeyetestapp.models.LightingStatus
import com.bioeye.bioeyetestapp.models.Status
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Fragment responsible for checking lighting conditions before face recognition.
 */
class CheckFragment : Fragment() {

    // SharedViewModel associated with the MainActivity
    private val sharedViewModel by activityViewModel<MainViewModel>()

    // ViewModel specific to the CheckFragment
    private val viewModel: CheckViewModel by viewModel()

    // View binding instance for the fragment
    private var _binding: FragmentCheckBinding? = null
    private val binding get() = _binding!!

    /**
     * Inflates the view for this fragment.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCheckBinding.inflate(inflater, container, false)
        return binding.root
    }

    /**
     * Cleans up resources when the view is destroyed.
     */
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /**
     * Initializes the UI components.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initObservers()
    }

    /**
     * Initializes the UI components.
     */
    private fun initUI() {
        binding.submit.setListener(object : PrimaryButton.PrimaryButtonListener {
            override fun onButtonClick() {
                viewModel.checkLighting()
            }
        })
    }

    /**
     * Initializes observers for observing lighting data changes.
     */
    private fun initObservers() {
        viewLifecycleOwner.lifecycleScope.launch {
            // Repeat the observer on the STARTED lifecycle state
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                // Launch a coroutine to collect lighting data changes
                launch {
                    viewModel.lightingData.collect { uiState ->
                        // Handle different UI states based on the data status
                        when (uiState.status) {
                            Status.LOADING -> handleProgressBar(true)
                            Status.SUCCESS -> {
                                handleProgressBar(false)
                                handleLightingData(uiState.data)
                            }

                            Status.ERROR -> {
                                handleProgressBar(false)
                                showError(uiState.message)
                            }

                            else -> { /* Do nothing for other states */
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Handles the visibility of the progress bar based on the loading state.
     *
     * @param isProgress Boolean indicating whether progress should be shown.
     */
    private fun handleProgressBar(isProgress: Boolean) {
        binding.progressBar.handleProgress(isProgress)
    }

    /**
     * Shows an error message using a Toast.
     *
     * @param message The error message to display.
     */
    private fun showError(message: String?) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    /**
     * Handles different lighting states and updates the UI accordingly.
     *
     * @param data The lighting status to be handled.
     */
    private fun handleLightingData(data: LightingStatus?) {
        data?.let {
            when (it) {
                LightingStatus.LIGHTING_OPTIMAL -> sharedViewModel.openFaceRecognition()
                LightingStatus.LIGHTING_DARK -> showDarkState()
                LightingStatus.LIGHTING_BRIGHT -> showBrightState()
                LightingStatus.LIGHTING_CHECK -> hideState()
            }
        }
    }

    /**
     * Hides the layout to indicate no specific lighting state.
     */
    private fun hideState() {
        binding.checkLayout.isVisible = false
    }

    /**
     * Shows the layout with dark lighting state.
     */
    private fun showDarkState() {
        binding.apply {
            checkLayout.isVisible = true
            title.text = getString(R.string.screen_check_lighting_dark_title)
            subtitle.text = getString(R.string.screen_check_lighting_dark_subtitle)
        }
    }

    /**
     * Shows the layout with bright lighting state.
     */
    private fun showBrightState() {
        binding.apply {
            checkLayout.isVisible = true
            title.text = getString(R.string.screen_check_lighting_bright_title)
            subtitle.text = getString(R.string.screen_check_lighting_bright_subtitle)
        }
    }
}