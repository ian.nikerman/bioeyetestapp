package com.bioeye.bioeyetestapp.helpers.camerax

import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.lifecycle.LifecycleOwner
import com.bioeye.bioeyetestapp.helpers.faceDetection.FaceContourDetectionProcessor
import com.bioeye.bioeyetestapp.models.FaceStatus
import com.google.common.util.concurrent.ListenableFuture
import timber.log.Timber
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * FaceRecognitionProviderClient is a class that manages the setup and execution of face recognition using CameraX.
 * It provides methods for requesting and removing face recognition updates, and it communicates with a FaceRecognitionCallback
 * to report recognition status changes.
 */
class FaceRecognitionProviderClient {

    // Executor service for camera operations
    private var cameraExecutor: ExecutorService? = null

    /**
     * An interface for receiving face recognition status changes.
     */
    interface FaceRecognitionCallback {
        fun onChangeState(status: FaceStatus)
        fun onCameraFailed(message: String)
    }

    /**
     * Requests face recognition updates using CameraX and starts the recognition process.
     *
     * @param executor An executor for background tasks.
     * @param cameraProviderFuture A ListenableFuture for obtaining the camera provider.
     * @param finderView A PreviewView for displaying the camera preview.
     * @param lifecycleOwner The LifecycleOwner associated with the camera view.
     * @param callBack The callback to receive recognition status changes.
     */
    fun requestRecognitionUpdates(
        executor: Executor,
        cameraProviderFuture: ListenableFuture<ProcessCameraProvider>,
        finderView: PreviewView,
        lifecycleOwner: LifecycleOwner,
        callBack: FaceRecognitionCallback
    ) {
        Timber.tag(TAG).d(::requestRecognitionUpdates.name)

        // Add a listener to the cameraProviderFuture
        cameraProviderFuture.addListener({
            val preview = Preview.Builder().build()

            // Create a single-threaded executor for camera operations
            cameraExecutor = Executors.newSingleThreadExecutor()

            cameraExecutor?.let { camExecutor ->
                val imageAnalyzer = ImageAnalysis.Builder()
                    .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                    .build()
                    .also {
                        it.setAnalyzer(
                            camExecutor,
                            FaceContourDetectionProcessor(object :
                                FaceContourDetectionProcessor.FaceContourDetectionCallback {
                                override fun onChangeState(status: FaceStatus) {
                                    callBack.onChangeState(status)
                                }
                            })
                        )
                    }
                val cameraSelector = CameraSelector.Builder()
                    .requireLensFacing(CameraSelector.LENS_FACING_FRONT)
                    .build()

                val cameraProvider = cameraProviderFuture.get()
                try {
                    cameraProvider?.unbindAll()
                    cameraProvider?.bindToLifecycle(
                        lifecycleOwner,
                        cameraSelector,
                        preview,
                        imageAnalyzer
                    )
                    preview.setSurfaceProvider(
                        finderView.surfaceProvider
                    )
                } catch (e: Exception) {
                    Timber.tag(TAG).e("RequestRecognitionUpdates failed: $e")
                    callBack.onCameraFailed(e.message.toString())
                }
            }
        }, executor)
    }

    /**
     * Removes face recognition updates and shuts down the camera executor.
     */
    fun removeRecognitionUpdates() {
        Timber.tag(TAG).d(::removeRecognitionUpdates.name)

        // Shutdown the camera executor to stop recognition
        cameraExecutor?.shutdown()
    }

    companion object {
        private val TAG = FaceRecognitionProviderClient::class.java.simpleName
    }
}