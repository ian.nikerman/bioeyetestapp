package com.bioeye.bioeyetestapp.helpers.camerax

import android.graphics.Rect
import androidx.annotation.OptIn
import androidx.camera.core.ExperimentalGetImage
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.bioeye.bioeyetestapp.common.Constants.RECOGNITION_DELAY_MS
import com.bioeye.bioeyetestapp.common.Constants.SNAPSHOT_COUNT_MAX
import com.google.android.gms.tasks.Task
import com.google.mlkit.vision.common.InputImage

/**
 * BaseImageAnalyzer is an abstract class that serves as the base for image analysis using the CameraX library.
 * It defines the core logic for analyzing images and provides hooks for custom analysis implementation.
 *
 * @param T The type of results produced by the image analysis.
 */
abstract class BaseImageAnalyzer<T> : ImageAnalysis.Analyzer {

    companion object {
        private var snapshotCounter = 0
        private var lastSnapshotTimeMs = 0L
    }

    // Constructor that initializes the time tracking variables
    init {
        snapshotCounter = 0
        lastSnapshotTimeMs = System.currentTimeMillis()
    }

    /**
     * Analyzes an image provided by the CameraX library.
     *
     * @param imageProxy The ImageProxy object containing the captured image.
     */
    @OptIn(ExperimentalGetImage::class)
    override fun analyze(imageProxy: ImageProxy) {
        val currentTimeMillis = System.currentTimeMillis()

        when {
            snapshotCounter == SNAPSHOT_COUNT_MAX -> {
                // If the maximum allowed number of image snapshots has been reached, finish the analysis.

                onFinish()
                imageProxy.close()
                return
            }

            currentTimeMillis - lastSnapshotTimeMs >= RECOGNITION_DELAY_MS -> {
                // Check if enough time has passed since the last snapshot for recognition

                snapshotCounter += 1
                lastSnapshotTimeMs = currentTimeMillis

                val mediaImage = imageProxy.image

                if (mediaImage != null) {
                    // Create an InputImage from the captured media image
                    val inputImage = InputImage.fromMediaImage(
                        mediaImage,
                        imageProxy.imageInfo.rotationDegrees
                    )

                    // Perform the actual image analysis and handle the results
                    detectInImage(inputImage)
                        .addOnSuccessListener { results ->
                            onSuccess(results, imageProxy.cropRect)
                            imageProxy.close()
                        }
                        .addOnFailureListener {
                            onFailure(it)
                            imageProxy.close()
                        }
                } else {
                    // Handle the case where the mediaImage is null
                    imageProxy.close()
                }
            }

            else -> {
                // Close the imageProxy if the delay threshold is not met
                imageProxy.close()
            }
        }
    }

    /**
     * Abstract method for performing image analysis. Subclasses must implement this method.
     *
     * @param image The InputImage to be analyzed.
     * @return A Task representing the analysis process.
     */
    protected abstract fun detectInImage(image: InputImage): Task<T>

    /**
     * Abstract method called when the image analysis should be stopped.
     */
    abstract fun stop()

    /**
     * Abstract method called when the analysis is successful, providing the results and the image crop rectangle.
     *
     * @param results The results of the analysis.
     * @param rect The crop rectangle of the image.
     */
    protected abstract fun onSuccess(results: T, rect: Rect)

    /**
     * Abstract method called when the analysis encounters an error.
     *
     * @param e The exception representing the analysis failure.
     */
    protected abstract fun onFailure(e: Exception)

    /**
     * Abstract method called when the image analysis session is finished.
     */
    protected abstract fun onFinish()
}

