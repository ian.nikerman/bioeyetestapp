package com.bioeye.bioeyetestapp.helpers.faceDetection

import android.graphics.Rect
import com.bioeye.bioeyetestapp.helpers.camerax.BaseImageAnalyzer
import com.bioeye.bioeyetestapp.models.FaceDimensions
import com.bioeye.bioeyetestapp.models.FaceStatus
import com.google.android.gms.tasks.Task
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import timber.log.Timber

/**
 * FaceContourDetectionProcessor is a class responsible for face detection and analysis using the CameraX library
 * in combination with Google's ML Kit Vision library. It extends BaseImageAnalyzer and provides custom logic
 * for face recognition and status reporting.
 *
 * @param callback The callback interface for receiving recognition status updates.
 */
class FaceContourDetectionProcessor(
    private val callback: FaceContourDetectionCallback
) :
    BaseImageAnalyzer<List<Face>>() {

    /**
     * An interface for receiving face contour detection status changes.
     */
    interface FaceContourDetectionCallback {
        fun onChangeState(status: FaceStatus)
    }

    // Configuration options for the face detector
    private val realTimeOpts = FaceDetectorOptions.Builder()
        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
        .setContourMode(FaceDetectorOptions.CONTOUR_MODE_NONE)
        .build()

    // Create an instance of the face detector
    private val detector = FaceDetection.getClient(realTimeOpts)

    /**
     * Perform face detection in the provided input image.
     *
     * @param image The InputImage to be analyzed for face detection.
     * @return A Task representing the result of the face detection process.
     */
    override fun detectInImage(image: InputImage): Task<List<Face>> {
        Timber.tag(TAG).d(::detectInImage.name)
        return detector.process(image)
    }

    /**
     * Stop the face detection process and close the face detector.
     */
    override fun stop() {
        Timber.tag(TAG).d(::stop.name)
        detector.close()
    }

    /**
     * Process the results of face detection and determine face recognition status.
     *
     * @param results The list of detected faces.
     * @param rect The crop rectangle of the analyzed image.
     */
    override fun onSuccess(
        results: List<Face>,
        rect: Rect
    ) {
        Timber.tag(TAG).d(::onSuccess.name)
        if (results.isNotEmpty()) {
            results.forEach { face ->
                val faceDimensions = getFaceDimensions(face)
                when {
                    checkIsTooFar(rect, faceDimensions) -> {
                        callback.onChangeState(FaceStatus.NOT_VALID)
                    }

                    checkIsNotCentered(rect, faceDimensions) -> {
                        callback.onChangeState(FaceStatus.NOT_VALID)
                    }

                    else -> {
                        callback.onChangeState(FaceStatus.VALID)
                    }
                }
            }
        } else {
            callback.onChangeState(FaceStatus.NOT_VALID)
            Timber.tag(TAG).d("${::onSuccess.name} empty results")
        }
    }

    /**
     * Check if the detected face is not centered within the provided rectangle.
     *
     * @param rect The crop rectangle.
     * @param faceDimensions The dimensions of the detected face.
     * @return True if the face is not centered, false otherwise.
     */
    private fun checkIsNotCentered(
        rect: Rect,
        faceDimensions: FaceDimensions
    ): Boolean {
        val width = rect.width()
        val height = rect.height()

        return faceDimensions.left < 0 || faceDimensions.right > width ||
                faceDimensions.top < 0 || faceDimensions.bottom > height
    }

    /**
     * Check if the detected face is too far from the center of the provided rectangle.
     *
     * @param rect The crop rectangle.
     * @param faceDimensions The dimensions of the detected face.
     * @return True if the face is too far from the center, false otherwise.
     */
    private fun checkIsTooFar(
        rect: Rect,
        faceDimensions: FaceDimensions
    ): Boolean {
        val screenPercentage = 0.4f
        val width = rect.width()
        val height = rect.height()

        return (faceDimensions.bottom - faceDimensions.top <= height * screenPercentage ||
                faceDimensions.right - faceDimensions.left <= width * screenPercentage)
    }

    /**
     * Calculate and return the dimensions of a detected face.
     *
     * @param face The detected face.
     * @return FaceDimensions containing the dimensions and position of the face.
     */
    private fun getFaceDimensions(face: Face): FaceDimensions {
        val x = face.boundingBox.centerX().toFloat()
        val y = face.boundingBox.centerY().toFloat()
        return FaceDimensions(
            x = x,
            y = y,
            left = x - face.boundingBox.width() / 2.0f,
            top = y - face.boundingBox.height() / 2.0f,
            right = x + face.boundingBox.width() / 2.0f,
            bottom = y + face.boundingBox.height() / 2.0f
        )
    }

    /**
     * Handle the failure of the face detection process.
     *
     * @param e The exception representing the failure.
     */
    override fun onFailure(e: Exception) {
        Timber.tag(TAG).d("${::onFailure.name} : $e")
        callback.onChangeState(FaceStatus.NOT_VALID)
    }

    /**
     * Handle the completion of the face recognition session, close the detector, and report the final status.
     */
    override fun onFinish() {
        Timber.tag(TAG).d(::onFinish.name)
        detector.close()
        callback.onChangeState(FaceStatus.EMPTY)
    }

    companion object {
        private val TAG = FaceContourDetectionProcessor::class.java.simpleName
    }
}
