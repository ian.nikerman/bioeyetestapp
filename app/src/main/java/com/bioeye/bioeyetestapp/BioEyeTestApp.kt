package com.bioeye.bioeyetestapp

import android.app.Application
import com.bioeye.bioeyetestapp.di.appModule
import com.bioeye.bioeyetestapp.di.servicesModule
import com.google.android.datatransport.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.Level
import timber.log.Timber

/**
 * Main application class for the BioEye TestApp.
 *
 * This class initializes the application, sets up logging using Timber, and configures Koin for dependency injection.
 */

class BioEyeTestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        setupLogger()
        setupKoin()
    }

    /**
     * Sets up logging using Timber. In this case, it plants a Timber DebugTree for logging.
     */
    private fun setupLogger() {
        Timber.plant(Timber.DebugTree())
    }

    /**
     * Sets up Koin for dependency injection.
     */
    private fun setupKoin() {
        startKoin {
            androidContext(this@BioEyeTestApp)
            // Configures the Koin logger level based on the BuildConfig (DEBUG or RELEASE).
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            // Loads modules for dependency injection: appModule and servicesModule.
            modules(appModule, servicesModule)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        // Stops Koin when the application terminates.
        stopKoin()
    }
}