package com.bioeye.bioeyetestapp.models

import java.io.File

/**
 * Data class representing a summary of the recognition session.
 *
 * @property sessionTime The total time of the recognition session.
 * @property detectedTime The time during which a face was detected.
 * @property noFaceTime The time during which no face was detected.
 */
data class Summary(
    val sessionTime: Int,
    val detectedTime: Int,
    val noFaceTime: Int
)

/**
 * Data class representing a share file, typically for sharing the summary data (e.g., a CSV file).
 *
 * @property file The CSV file to be shared.
 */
data class ShareFileData(
    val file: File?
)