package com.bioeye.bioeyetestapp.models

/**
 * Data class representing permission status and type.
 *
 * @property permissionType The type of permission (e.g., CAMERA).
 * @property permissionStatus The status of the permission (e.g., GRANTED, DENIED).
 */
data class PermissionLockData(
    val permissionType: PermissionType,
    val permissionStatus: PermissionStatus
)

/**
 * An enum class representing the possible status values for a response.
 */
enum class PermissionStatus {
    GRANTED, DENIED
}

/**
 * An enum class representing the types of permissions.
 */
enum class PermissionType {
    CAMERA
}