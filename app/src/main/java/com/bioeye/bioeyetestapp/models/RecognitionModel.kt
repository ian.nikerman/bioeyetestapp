package com.bioeye.bioeyetestapp.models

/**
 * An enum class representing different face recognition statuses.
 */
enum class FaceStatus {
    EMPTY,     // No face was recognized.
    NOT_VALID, // A face was recognized but not considered valid.
    VALID      // A valid face recognition occurred.
}

/**
 * A data class representing recognition data including a timestamp and face detection status.
 *
 * @property timestamp The timestamp of the recognition data.
 * @property is_face_detected An integer indicating whether a face was detected (1 for detected, 0 for not detected).
 */
data class RecognitionData(
    val timestamp: String,
    val is_face_detected: Int
)

/**
 * A data class representing the dimensions and position of a detected face.
 *
 * @property x The x-coordinate of the face's center.
 * @property y The y-coordinate of the face's center.
 * @property left The left boundary of the face.
 * @property top The top boundary of the face.
 * @property right The right boundary of the face.
 * @property bottom The bottom boundary of the face.
 */
data class FaceDimensions(
    val x: Float,
    val y: Float,
    val left: Float,
    val top: Float,
    val right: Float,
    val bottom: Float
)