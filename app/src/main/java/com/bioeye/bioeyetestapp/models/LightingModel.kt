package com.bioeye.bioeyetestapp.models

/**
 * An enum class representing different lighting statuses for face recognition.
 */
enum class LightingStatus {
    LIGHTING_CHECK,   // Indicates a lighting check.
    LIGHTING_OPTIMAL, // Indicates optimal lighting conditions.
    LIGHTING_BRIGHT,  // Indicates bright lighting conditions.
    LIGHTING_DARK     // Indicates dark lighting conditions.
}