package com.bioeye.bioeyetestapp.models

/**
 * A data class that represents the state of a response.
 * It includes status information, the data (if any), and an optional message.
 *
 * @param T The type of data contained in the response.
 * @property status The status of the response, which can be one of the [Status] enum values.
 * @property data The data contained in the response, which can be of any type T.
 * @property message An optional message associated with the response.
 */
data class ResponseState<out T>(val status: Status, val data: T?, val message: String?) {

    companion object {

        /**
         * Creates a ResponseState with the [Status.EMPTY] status.
         *
         * @param T The type of data to be associated with the response.
         * @return An empty response state.
         */
        fun <T> onEmpty(): ResponseState<T> {
            return ResponseState(Status.EMPTY, null, "")
        }

        /**
         * Creates a ResponseState with the [Status.SUCCESS] status and the provided data.
         *
         * @param T The type of data to be associated with the response.
         * @param data The data to be included in the response.
         * @return A successful response state with the provided data.
         */
        fun <T> success(data: T?): ResponseState<T> {
            return ResponseState(Status.SUCCESS, data, null)
        }

        /**
         * Creates a ResponseState with the [Status.ERROR] status and the provided error message.
         *
         * @param T The type of data to be associated with the response.
         * @param msg The error message describing the response.
         * @return An error response state with the provided error message.
         */
        fun <T> error(msg: String): ResponseState<T> {
            return ResponseState(Status.ERROR, null, msg)
        }

        /**
         * Creates a ResponseState with the [Status.LOADING] status.
         *
         * @param T The type of data to be associated with the response.
         * @return A loading response state.
         */
        fun <T> loading(): ResponseState<T> {
            return ResponseState(Status.LOADING, null, null)
        }
    }
}

/**
 * An enum class that represents the possible status values for a response.
 */
enum class Status {
    EMPTY, SUCCESS, ERROR, LOADING
}
